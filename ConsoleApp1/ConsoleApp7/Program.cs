﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp7
{
    //클래스정의
    public class Employee
    {
        public int BirthYear;
        public string Name;
    }


    class Program
    {
        static void Main(string[] args)
        {
            //인스턴스생성
            Employee emp1 = new Employee();
            emp1.Name = "김도현";
            emp1.BirthYear = 1983;

            Employee emp2 = emp1;

            Console.WriteLine("emp1.BirthYear : {0}", emp1.BirthYear);    //1983
            Console.WriteLine("emp2.BirthYear : {0}", emp2.BirthYear);    //1983

            emp1.BirthYear = 1978;     //emp1.BirthYear값 변경
            Console.WriteLine("===emp1.BirthYear =1978값 변경 ===");
            Console.WriteLine("emp1.BirthYear : {0}", emp1.BirthYear);  //1978
            Console.WriteLine("emp2.BirthYear : {0}", emp2.BirthYear);  //1978

        }
    }
}
