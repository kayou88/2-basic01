﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1
{
    class Program
    {
        /// <summary>
        /// 클래스/메소드 등의 XML주석
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //단일 라인 주석
            Console.WriteLine("Hello world !");
        }
    }
}
