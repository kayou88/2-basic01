﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            //int에 null 가능 하도록 선언
            int? number = null;
            number= 10,                        //암시적으로 값 설정

            if (number.HasVlue)               //number == null 으로 비교 가능
            {
                //Value 프로퍼티를 사용하지 않고 바로 변수명만 적어도 가능
                Console.WriteLine(number.Value);     //Console.WriteLine(number);

            }
            else
            {
                Console.WriteLine("값이 null 입니다.");
            }
        }
    }
}
