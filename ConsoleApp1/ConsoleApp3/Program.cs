﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            //int 형 선언
            int number1 = 123;      //C#표현, System.Int32와 동일
            System.Int32 number2 = 123;       //.NET Framework 표현, int와 동일
            Console.WriteLine("number1 : {0}", number1);
            Console.WriteLine("number1 : {0}", number2);

            //double 형 선언
            double number3 = 123D;   //"D"형식 접미사를 통한 명시적 선언
            double number4 = 123;    //INT 형식을 double형식으로 암시적으로 형
            Console.WriteLine("number3 : {0}", number3);
            Console.WriteLine("number4 : {0}", number4);

            //형 선언 후 초기화 하지 않음
            //int number5;       //사용전에 초기화 하지 않으면 컴파일 에러
            //Console.WriteLine("number5 : {0}",number5);





        }
    }
}
