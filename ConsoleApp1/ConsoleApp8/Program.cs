﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            string value1 = "값1";
            var value2 = "값2";
            dynamic value3 = "값3";

            Console.WriteLine("value1 : {0}", value1);
            Console.WriteLine("value2 : {0}", value2);
            Console.WriteLine("value3 : {0}", value3);

        }
    }
}
